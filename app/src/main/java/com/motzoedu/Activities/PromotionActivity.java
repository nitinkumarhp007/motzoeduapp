package com.motzoedu.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.motzoedu.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PromotionActivity extends AppCompatActivity {
    PromotionActivity context;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.link)
    TextView link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        ButterKnife.bind(this);

        setToolbar();

        if (!getIntent().getStringExtra("promotion_text").isEmpty())
            name.setText(getIntent().getStringExtra("promotion_text"));
        if (!getIntent().getStringExtra("url").isEmpty())
            link.setText(getIntent().getStringExtra("url"));

        if (!getIntent().getStringExtra("url").isEmpty())
            link.setVisibility(View.VISIBLE);
        else
            link.setVisibility(View.INVISIBLE);

        context = PromotionActivity.this;

    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Alimentazione Promozioni");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.mm2);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.link)
    public void onClick() {
        Intent i2 = new Intent(context, WebViewActivity.class);
        i2.putExtra("url", getIntent().getStringExtra("url"));
        startActivity(i2);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
