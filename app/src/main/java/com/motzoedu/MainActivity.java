package com.motzoedu;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.motzoedu.Activities.FoodDetailActivity;
import com.motzoedu.Activities.FoodListActivity;
import com.motzoedu.Activities.ProfileActivity;
import com.motzoedu.Activities.PromotionActivity;
import com.motzoedu.Activities.WebViewActivity;
import com.motzoedu.Util.ConnectivityReceiver;
import com.motzoedu.Util.Parameters;
import com.motzoedu.Util.SavePref;
import com.motzoedu.Util.util;
import com.motzoedu.parser.AllAPIS;
import com.motzoedu.parser.GetAsync;
import com.motzoedu.parser.GetAsyncGet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity {
    MainActivity context;
    SavePref savePref;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.profile)
    ImageView profile;
    @BindView(R.id.today)
    Button today;
    @BindView(R.id.tomorrow)
    Button tomorrow;
    @BindView(R.id.day_after_tomorrow)
    Button dayAfterTomorrow;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.v3)
    View v3;
    @BindView(R.id.breakfast_layout)
    RelativeLayout breakfastLayout;
    @BindView(R.id.snacks_layout)
    CardView snacksLayout;
    @BindView(R.id.lunch_layout)
    RelativeLayout lunchLayout;
    @BindView(R.id.snacks_layout_2)
    RelativeLayout snacksLayout2;
    @BindView(R.id.dinner_layout)
    RelativeLayout dinnerLayout;
    @BindView(R.id.youtube)
    ImageView youtube;
    @BindView(R.id.food_promotion)
    ImageView foodPromotion;
    @BindView(R.id.pic_1)
    ImageView pic1;
    @BindView(R.id.pic_2)
    ImageView pic2;
    @BindView(R.id.pic_3)
    ImageView pic3;
    @BindView(R.id.pic_4)
    ImageView pic4;
    @BindView(R.id.pic_5)
    ImageView pic5;

    String day_type = "1", promotion_text = "", promotion_url = "";
    @BindView(R.id.topPanel_top)
    LinearLayout topPanelTop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = MainActivity.this;
        savePref = new SavePref(context);

    //    Log.e("token___", "token " + savePref.getDeviceToken(context, "token"));
        Log.e("token___", "token " + savePref.getAuthorization_key());

        if (ConnectivityReceiver.isConnected()) {
            GETTODAYMEALIMAGES("1", true);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    @OnClick({R.id.menu, R.id.profile, R.id.today, R.id.tomorrow, R.id.day_after_tomorrow, R.id.pic_1, R.id.breakfast_layout, R.id.snacks_layout, R.id.lunch_layout, R.id.snacks_layout_2, R.id.dinner_layout, R.id.youtube, R.id.food_promotion})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu:
                startActivity(new Intent(this, ProfileActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.profile:
                break;
            case R.id.today:
                day_type = "1";
                if (ConnectivityReceiver.isConnected()) {
                    GETTODAYMEALIMAGES("1", false);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                today.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                tomorrow.setTextColor(getResources().getColor(R.color.black));
                dayAfterTomorrow.setTextColor(getResources().getColor(R.color.black));
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                v3.setBackgroundColor(getResources().getColor(R.color.white));
                break;
            case R.id.tomorrow:
                day_type = "2";
                if (ConnectivityReceiver.isConnected()) {
                    GETTODAYMEALIMAGES("2", false);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                today.setTextColor(getResources().getColor(R.color.black));
                tomorrow.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                dayAfterTomorrow.setTextColor(getResources().getColor(R.color.black));
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v3.setBackgroundColor(getResources().getColor(R.color.white));
                break;
            case R.id.day_after_tomorrow:
                day_type = "3";
                if (ConnectivityReceiver.isConnected()) {
                    GETTODAYMEALIMAGES("3", false);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                today.setTextColor(getResources().getColor(R.color.black));
                tomorrow.setTextColor(getResources().getColor(R.color.black));
                dayAfterTomorrow.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                v3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                break;
            case R.id.pic_1:
                Intent intent = new Intent(this, FoodListActivity.class);
                intent.putExtra("type", "1");
                intent.putExtra("day_type", day_type);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.breakfast_layout:
                Intent intent2 = new Intent(this, FoodListActivity.class);
                intent2.putExtra("type", "1");
                intent2.putExtra("day_type", day_type);
                startActivity(intent2);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.snacks_layout:
                Intent intent3 = new Intent(this, FoodListActivity.class);
                intent3.putExtra("type", "2");
                intent3.putExtra("day_type", day_type);
                startActivity(intent3);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.lunch_layout:
                Intent intent4 = new Intent(this, FoodListActivity.class);
                intent4.putExtra("type", "3");
                intent4.putExtra("day_type", day_type);
                startActivity(intent4);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.snacks_layout_2:
                Intent intent5 = new Intent(this, FoodListActivity.class);
                intent5.putExtra("type", "4");
                intent5.putExtra("day_type", day_type);
                startActivity(intent5);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.dinner_layout:
                Intent intent6 = new Intent(this, FoodListActivity.class);
                intent6.putExtra("type", "5");
                intent6.putExtra("day_type", day_type);
                startActivity(intent6);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.youtube:
                Intent i1 = new Intent(this, WebViewActivity.class);
                i1.putExtra("url", "https://www.motzoedu.it/sponsor/");
                startActivity(i1);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.food_promotion:
              /*  Intent i111 = new Intent(this, PromotionActivity.class);
                i111.putExtra("promotion_text", promotion_text);
                i111.putExtra("url", promotion_url);
                startActivity(i111);

               // share_wa("com.whatsapp");*/

                Intent i = new Intent(this, WebViewActivity.class);
                i.putExtra("url", "https://www.motzoedu.it");
                startActivity(i);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void GETTODAYMEALIMAGES(String type, boolean is_first) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TYPE, type);
        formBuilder.addFormDataPart(Parameters.DATE, date);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context,
                AllAPIS.GETTODAYMEALIMAGES + "?type=" + type + "&date=" + date + "&userId=" + savePref.getID(), formBody) {
            @Override
            public void getValueParse(String result) {
                if (type.equals("1")) {
                   /* if (is_first) {
                        Log.e("hitttttt", "yes");
                        mDialog.dismiss();
                        GETTODAYMEALIMAGES("1", false);
                    } else {
                        GETPROMOZIONI(mDialog);
                    }*/

                    GETPROMOZIONI(mDialog);

                } else
                    mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("body");

                            if (!body.getJSONObject(0).getString("image").isEmpty()) {
                                //Glide.with(context).load(body.getJSONObject(0).getString("image")).into(pic1);

                                Picasso.with(context).load(body.getJSONObject(0).getString("image")).into(pic1);

                                // Glide.with(context).load(R.drawable.pp).into(pic1);
                            } else {
                                Picasso.with(context).load(R.drawable.p1).into(pic1);
                            }
                            if (!body.getJSONObject(1).getString("image").isEmpty()) {
                                Picasso.with(context).load(body.getJSONObject(1).getString("image")).into(pic2);
                            } else {
                                Picasso.with(context).load(R.drawable.p1).into(pic2);
                            }
                            if (!body.getJSONObject(2).getString("image").isEmpty()) {
                                Picasso.with(context).load(body.getJSONObject(2).getString("image")).into(pic3);
                            } else {
                                Picasso.with(context).load(R.drawable.p2).into(pic3);
                            }
                            if (!body.getJSONObject(3).getString("image").isEmpty()) {
                                Picasso.with(context).load(body.getJSONObject(3).getString("image")).into(pic4);
                            } else {
                                Picasso.with(context).load(R.drawable.p1).into(pic4);
                            }
                            if (!body.getJSONObject(4).getString("image").isEmpty()) {
                                Picasso.with(context).load(body.getJSONObject(4).getString("image")).into(pic5);
                            } else {
                                Picasso.with(context).load(R.drawable.p2).into(pic5);
                            }

                            topPanelTop.setVisibility(View.VISIBLE);
                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void GETPROMOZIONI(ProgressDialog mDialog) {
       /* final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();*/
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.GETPROMOZIONI, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            promotion_text = body.getString("promozioniText");
                            promotion_url = body.getString("promozioniLink");


                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shareopencheck();
    }

    private void shareopencheck() {
        try {
            //check_notification_flag = true;
            final Intent intent = getIntent();
            Uri data = intent.getData();
            Log.e("data_share", data.toString());
            String post_id = data.toString().substring(data.toString().lastIndexOf("/") + 1);
            Log.e("post_id", post_id);

            // util.IOSDialog(context,"id:"+post_id);

            Intent intent1 = new Intent(context, FoodDetailActivity.class);
            intent1.putExtra("is_from_share", true);
            intent1.putExtra("food_id", post_id);
            context.startActivity(intent1);

        } catch (Exception e) {

        }
    }
}
