package com.motzoedu.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.motzoedu.MainActivity;
import com.motzoedu.R;
import com.motzoedu.Util.ConnectivityReceiver;
import com.motzoedu.Util.Parameters;
import com.motzoedu.Util.SavePref;
import com.motzoedu.Util.util;
import com.motzoedu.parser.AllAPIS;
import com.motzoedu.parser.GetAsync;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.tvbarthel.intentshare.IntentShare;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FoodDetailActivity extends AppCompatActivity {
    FoodDetailActivity context;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.title_)
    TextView title_text;
    private SavePref savePref;

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.descrption)
    TextView descrption;
    @BindView(R.id.back_button)
    ImageView backButton;


    CallbackManager callbackManager;
    ShareDialog shareDialog;
    @BindView(R.id.fb)
    ImageView fb;
    @BindView(R.id.whatsapp)
    ImageView whatsapp;
    @BindView(R.id.gamil)
    ImageView gamil;
    @BindView(R.id.twitter)
    ImageView twitter;

    String food_id = "";
    boolean is_from_share = false;

    private String selectedimage = "";
    Uri fileUri;
    Uri resultUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        ButterKnife.bind(this);

        context = FoodDetailActivity.this;
        savePref = new SavePref(context);

        is_from_share = getIntent().getBooleanExtra("is_from_share", false);
        food_id = getIntent().getStringExtra("food_id");


        shareDialog = new ShareDialog(context);
        callbackManager = CallbackManager.Factory.create();

        if (ConnectivityReceiver.isConnected())
            FOODDETAIL_API();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


        title_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(context);*/
            }
        });


    }

    private void FOODDETAIL_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, food_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FOODDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            name.setText(body.getString("name"));
                            descrption.setText(body.getString("description"));

                            selectedimage = body.getString("image");
                            Glide.with(context).load(selectedimage).into(image);


                            scrollView.setVisibility(View.VISIBLE);

                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void share_pin() {
        String shareUrl = "http://drmotzo.com/api/share/" + food_id;
        String mediaUrl = selectedimage;
        String description = "Ecco uno dei consigli alimentari di oggi che puoi trovare sull'App Dr. Motzo";
        String url = String.format("https://www.pinterest.com/pin/create/button/?url=%s&media=%s&description=%s",
                urlEncode(shareUrl), urlEncode(mediaUrl), urlEncode(description));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        filterByPackageName(context, intent, "com.pinterest");
        context.startActivity(intent);
    }

    public static void filterByPackageName(Context context, Intent intent, String prefix) {
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(prefix)) {
                intent.setPackage(info.activityInfo.packageName);
                return;
            }
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("", "UTF-8 should always be supported", e);
            return "";
        }
    }

    private void share_wa(String package_name) {
        BitmapDrawable bitmapDrawable;
        Bitmap bitmap1;
        //write this code in your share button or function

        bitmapDrawable = (BitmapDrawable) image.getDrawable();// get the from imageview or use your drawable from drawable folder
        bitmap1 = bitmapDrawable.getBitmap();
        String imgBitmapPath = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap1, name.getText().toString(), null);
        Uri imgBitmapUri = Uri.parse(imgBitmapPath);
        //  String shareText="Share image and text";
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setPackage(package_name);
        // shareIntent.setPackage("com.facebook.katana");
        // shareIntent.setPackage("com.google.android.gm");
        // shareIntent.setPackage("com.whatsapp");
        // shareIntent.setPackage("com.twitter.android");
        // shareIntent.setType("*/*");
        // shareIntent.putExtra(Intent.EXTRA_TEXT, "http://drmotzo.com/api/share/" + foodModel.getId());
        shareIntent.putExtra(Intent.EXTRA_TITLE, "Ecco uno dei consigli alimentari di oggi che puoi trovare sull'App \"Dr. Motzo\"\n\nhttp://drmotzo.com/api/share/" + food_id);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Ecco uno dei consigli alimentari di oggi che puoi trovare sull'App \"Dr. Motzo\"\n\nhttp://drmotzo.com/api/share/" + food_id);
        // shareIntent.putExtra("com.pinterest.EXTRA_DESCRIPTION", "Ecco uno dei consigli alimentari di oggi che puoi trovare sull'App \"Dr. Motzo\"\n\nhttp://drmotzo.com/api/share/" + food_id);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri);

        shareIntent.setType("*/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            context.startActivity(Intent.createChooser(shareIntent, "Share Post External"));
        } catch (ActivityNotFoundException ex) {
            util.IOSDialog(context, "App have not been installed.");
        }
    }


    private void facebookwallshareTask(final View view) {
        BitmapDrawable bitmapDrawable;
        Bitmap bitmap1;
//write this code in your share button or function


        bitmapDrawable = (BitmapDrawable) image.getDrawable();// get the from imageview or use your drawable from drawable folder
        bitmap1 = bitmapDrawable.getBitmap();

        Bitmap image = bitmap1;
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();


        bitmap1 = bitmapDrawable.getBitmap();
        String imgBitmapPath = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap1, name.getText().toString(), null);
        Uri imgBitmapUri = Uri.parse(imgBitmapPath);


        shareDialog.registerCallback(callbackManager, new
                FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Log.e("ShareOnFacebook", "onSuccess: ");
                        util.showSnackBar(view, "You Have sucessfully Share CeedPage App on Facebook", context);
                    }

                    @Override
                    public void onCancel() {
                        Log.e("ShareOnFacebook", "onCancel: ");
                        util.showSnackBar(view, "You Have Cancel CeedPage App Share", context);
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("ShareOnFacebook", "onError: ");
                        util.showSnackBar(view, "Error in Sharing App", context);
                    }
                });
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    // .setContentTitle("Dr. Motzo App")
                    .setContentUrl(imgBitmapUri)
                    .setQuote("You are invited to download CeedPage. Google Play Store Url :-")
                    //  .setContentUrl(Uri.parse("http://drmotzo.com/api/share/" + foodModel.getId()))
                    .build();
            shareDialog.show(linkContent, ShareDialog.Mode.AUTOMATIC);
        }
    }

    @OnClick({R.id.back_button, R.id.fb, R.id.whatsapp, R.id.gamil, R.id.twitter})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.fb:
                fb_dialog();
                //share_wa("com.facebook.katana");
                break;
            case R.id.whatsapp:
                share_wa("com.whatsapp");
                break;
            case R.id.gamil:
                share_wa("com.google.android.gm");
                break;
            case R.id.twitter:
                share_wa("com.twitter.android");
                //share_wa("com.pinterest");
                // share_pin();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!is_from_share)
            super.onBackPressed();
        else {
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        }

    }


    private void fb_dialog() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        BitmapDrawable bitmapDrawable;
        Bitmap bitmap1;
//write this code in your share button or function

        bitmapDrawable = (BitmapDrawable) image.getDrawable();// get the from imageview or use your drawable from drawable folder
        bitmap1 = bitmapDrawable.getBitmap();
        String imgBitmapPath = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap1, name.getText().toString(), null);
        Uri imgBitmapUri = Uri.parse(imgBitmapPath);


        String path = MediaStore.Images.Media.insertImage(
                getContentResolver(), bitmap1, "IMG_" + System.currentTimeMillis(), null);


        ShareHashtag shareHashTag = new ShareHashtag.Builder().setHashtag("Ecco uno dei consigli alimentari di oggi che puoi trovare sull'App \"Dr. Motzo\"\n\nhttp://drmotzo.com/api/share/" + food_id).build();
        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                .setShareHashtag(shareHashTag)
                .setQuote(name.getText().toString())
                // .setContentUrl(imgBitmapUri)
                .setContentUrl(Uri.parse(selectedimage))
                .build();

        ShareDialog.show(FoodDetailActivity.this, shareLinkContent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();


                selectedimage = getAbsolutePath(this, resultUri);

                Glide.with(this).load(selectedimage).into(image);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}
