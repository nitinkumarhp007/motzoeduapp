package com.motzoedu.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.motzoedu.Activities.NotificationShowActivity;
import com.motzoedu.MainActivity;
import com.motzoedu.R;
import com.motzoedu.Util.SavePref;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    String message = "", friend_id = "", order_id = "", message_id = "", productId = "", product_name = "", notification_code = "", product_id = "", username = "", sender_id = "", chat_message, timeStamp;

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    NotificationManager notificationManager;
    Notification notification;
    String groupId = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());
        message = remoteMessage.getData().get("title");
       // notification_code = remoteMessage.getData().get("type");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        }

        if (notification_code.equals("2")) {
            sendNotification(getApplicationContext(), message);
        } else {
            sendNotification(getApplicationContext(), message);
        }
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
        Log.e("token___", token);
    }

    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;
        intent = new Intent(context, NotificationShowActivity.class);
        intent.putExtra("notification_code", notification_code);
        intent.putExtra("message", message);
        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    /*    Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);*/


        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);


    /*    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.noti_logo).setLargeIcon(icon1);;
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }


}
