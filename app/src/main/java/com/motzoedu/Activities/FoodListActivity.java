package com.motzoedu.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.motzoedu.Adapters.FoodListAdapter;
import com.motzoedu.ModelClasses.FoodModel;
import com.motzoedu.R;
import com.motzoedu.Util.ConnectivityReceiver;
import com.motzoedu.Util.Parameters;
import com.motzoedu.Util.SavePref;
import com.motzoedu.Util.util;
import com.motzoedu.parser.AllAPIS;
import com.motzoedu.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FoodListActivity extends AppCompatActivity {
    FoodListActivity context;
    @BindView(R.id.error_text)
    TextView errorText;
    private SavePref savePref;
    @BindView(R.id.today)
    Button today;
    @BindView(R.id.tomorrow)
    Button tomorrow;
    @BindView(R.id.day_after_tomorrow)
    Button dayAfterTomorrow;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.v3)
    View v3;
    @BindView(R.id.my_reccyclerView)
    RecyclerView myReccyclerView;

    ArrayList<FoodModel> list_today;
    ArrayList<FoodModel> list_tomorrow;
    ArrayList<FoodModel> list_dayaftertomorrow;

    String type = "", day_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        ButterKnife.bind(this);


        context = FoodListActivity.this;
        savePref = new SavePref(context);

        type = getIntent().getStringExtra("type");
        day_type = getIntent().getStringExtra("day_type");
        setToolbar();

        if (ConnectivityReceiver.isConnected()) {
            FOODLIST_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void FOODLIST_API() {
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, type); //1=>Colazione, 2=>Merenda Mattina,
        formBuilder.addFormDataPart(Parameters.DATE, date);
        // 3=>Pranzo, 4=>Merenda Pomeriggio, 5=>Cena
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.HOMELISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list_today = new ArrayList<>();
                if (list_today.size() > 0) {
                    list_today.clear();
                }
                list_tomorrow = new ArrayList<>();
                if (list_tomorrow.size() > 0) {
                    list_tomorrow.clear();
                }
                list_dayaftertomorrow = new ArrayList<>();
                if (list_dayaftertomorrow.size() > 0) {
                    list_dayaftertomorrow.clear();
                }
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            JSONArray today = body.getJSONArray("today");
                            for (int i = 0; i < today.length(); i++) {
                                JSONObject object = today.getJSONObject(i);
                                FoodModel foodModel = new FoodModel();
                                foodModel.setId(object.getString("id"));
                                foodModel.setName(object.getString("name"));
                                foodModel.setImage(object.getString("image"));
                                foodModel.setDescription(object.getString("description"));
                                list_today.add(foodModel);
                            }
                            JSONArray tomorrow = body.getJSONArray("tomorrow");
                            for (int i = 0; i < tomorrow.length(); i++) {
                                JSONObject object = tomorrow.getJSONObject(i);
                                FoodModel foodModel = new FoodModel();
                                foodModel.setId(object.getString("id"));
                                foodModel.setName(object.getString("name"));
                                foodModel.setImage(object.getString("image"));
                                foodModel.setDescription(object.getString("description"));
                                list_tomorrow.add(foodModel);
                            }
                            JSONArray dayAfterTomorrow = body.getJSONArray("dayAfterTomorrow");
                            for (int i = 0; i < dayAfterTomorrow.length(); i++) {
                                JSONObject object = dayAfterTomorrow.getJSONObject(i);
                                FoodModel foodModel = new FoodModel();
                                foodModel.setId(object.getString("id"));
                                foodModel.setName(object.getString("name"));
                                foodModel.setImage(object.getString("image"));
                                foodModel.setDescription(object.getString("description"));
                                list_dayaftertomorrow.add(foodModel);
                            }

                            if (day_type.equals("1")) {
                                if (list_today.size() > 0) {
                                    myReccyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myReccyclerView.setAdapter(new FoodListAdapter(context, list_today));
                                    myReccyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.INVISIBLE);
                                } else {
                                    myReccyclerView.setVisibility(View.GONE);
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            } else if (day_type.equals("2")) {
                                if (list_tomorrow.size() > 0) {
                                    myReccyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myReccyclerView.setAdapter(new FoodListAdapter(context, list_tomorrow));
                                    myReccyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.INVISIBLE);
                                } else {
                                    myReccyclerView.setVisibility(View.GONE);
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            } else if (day_type.equals("3")) {
                                if (list_dayaftertomorrow.size() > 0) {
                                    myReccyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myReccyclerView.setAdapter(new FoodListAdapter(context, list_dayaftertomorrow));
                                    myReccyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.INVISIBLE);
                                } else {
                                    myReccyclerView.setVisibility(View.GONE);
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            }

                            set_color();


                        } else {
                            util.showToast(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void set_color() {
        if (day_type.equals("1")) {
            today.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            tomorrow.setTextColor(getResources().getColor(R.color.black));
            dayAfterTomorrow.setTextColor(getResources().getColor(R.color.black));
            v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            v2.setBackgroundColor(getResources().getColor(R.color.white));
            v3.setBackgroundColor(getResources().getColor(R.color.white));
        } else if (day_type.equals("2")) {
            today.setTextColor(getResources().getColor(R.color.black));
            tomorrow.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            dayAfterTomorrow.setTextColor(getResources().getColor(R.color.black));
            v1.setBackgroundColor(getResources().getColor(R.color.white));
            v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            v3.setBackgroundColor(getResources().getColor(R.color.white));
        } else if (day_type.equals("3")) {
            today.setTextColor(getResources().getColor(R.color.black));
            tomorrow.setTextColor(getResources().getColor(R.color.black));
            dayAfterTomorrow.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            v1.setBackgroundColor(getResources().getColor(R.color.white));
            v2.setBackgroundColor(getResources().getColor(R.color.white));
            v3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }

    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        if (type.equals("1"))
            title.setText("Colazione");
        else if (type.equals("2"))
            title.setText("Merenda Mattina");
        else if (type.equals("3"))
            title.setText("Pranzo");
        else if (type.equals("4"))
            title.setText("Merenda Pomeriggio");
        else if (type.equals("5"))
            title.setText("Cena");


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.mm2);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @OnClick({R.id.today, R.id.tomorrow, R.id.day_after_tomorrow})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.today:
                today.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                tomorrow.setTextColor(getResources().getColor(R.color.black));
                dayAfterTomorrow.setTextColor(getResources().getColor(R.color.black));
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                v3.setBackgroundColor(getResources().getColor(R.color.white));

                if (list_today.size() > 0) {
                    myReccyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myReccyclerView.setAdapter(new FoodListAdapter(context, list_today));
                    myReccyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.INVISIBLE);
                } else {
                    myReccyclerView.setVisibility(View.GONE);
                    errorText.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tomorrow:
                today.setTextColor(getResources().getColor(R.color.black));
                tomorrow.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                dayAfterTomorrow.setTextColor(getResources().getColor(R.color.black));
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v3.setBackgroundColor(getResources().getColor(R.color.white));

                if (list_tomorrow.size() > 0) {
                    myReccyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myReccyclerView.setAdapter(new FoodListAdapter(context, list_tomorrow));
                    myReccyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.INVISIBLE);
                } else {
                    myReccyclerView.setVisibility(View.GONE);
                    errorText.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.day_after_tomorrow:
                today.setTextColor(getResources().getColor(R.color.black));
                tomorrow.setTextColor(getResources().getColor(R.color.black));
                dayAfterTomorrow.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                v3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

                if (list_dayaftertomorrow.size() > 0) {
                    myReccyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myReccyclerView.setAdapter(new FoodListAdapter(context, list_dayaftertomorrow));
                    myReccyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.INVISIBLE);
                } else {
                    myReccyclerView.setVisibility(View.GONE);
                    errorText.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
}
