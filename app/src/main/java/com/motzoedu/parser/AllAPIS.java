package com.motzoedu.parser;


public class AllAPIS {

    //public static final String BASE_URL = "http://3.6.100.60:9999/api/"; rj server
    public static final String BASE_URL = "http://drmotzo.com/api/";

    public static final String USERLOGIN = BASE_URL + "login";
    public static final String USER_SIGNUP = BASE_URL + "signup";
    public static final String VERIFY_OTP = BASE_URL + "forgotPassword";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "changePassword";
    public static final String EDIT_PROFILE = BASE_URL + "editProfile";
    public static final String TERMS = BASE_URL + "termsAndConditions";
    public static final String PRIVACYPOLICY = BASE_URL + "privacyPolicy";

    public static final String CONTACT_US = BASE_URL + "contactUs";
    public static final String HOMELISTING = BASE_URL + "homeListing";
    public static final String GETPROMOZIONI = BASE_URL + "getPromozioni";
    public static final String GETTODAYMEALIMAGES = BASE_URL + "getTodayMealImages";
    public static final String NOTIFICATIONS = BASE_URL + "getPushNotifications";
    public static final String FOODDETAIL = BASE_URL + "foodDetail";
}
