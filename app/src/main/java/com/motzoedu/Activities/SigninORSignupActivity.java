package com.motzoedu.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.motzoedu.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninORSignupActivity extends AppCompatActivity {

    @BindView(R.id.login)
    Button login;
    @BindView(R.id.signup)
    Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_orsignup);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.login, R.id.signup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.signup:
                startActivity(new Intent(this, SignUpActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }
}
